#! /usr/bin/env bash

echo '--- Swarm Init---'
docker swarm init --listen-addr $Cluster_Manager_IP:2377 --advertise-addr $Cluster_Manager_IP:2377
docker swarm join-token --quiet worker > /vagrant/token_node
